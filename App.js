import React from 'react'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Admin from './screens/Admin/Admin';
import User from './screens/User/User';
import Login from './screens/Login';
import AppProvider from './AppProvider';

const MainNavigator = createStackNavigator({
  Login: { screen: Login },
  Admin: { screen: Admin },
  User: { screen: User }
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});
const App = createAppContainer(MainNavigator);

const WrapperContext = (props) => {
  return (<AppProvider>
    <App {...props}></App>
  </AppProvider>)
}
export default WrapperContext;