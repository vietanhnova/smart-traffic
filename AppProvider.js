import React from 'react';
const AppContext = React.createContext({ plates: [], username: "" });


class AppProvider extends React.Component {
    static Consumer = AppContext.Consumer;
    state = {
        plates: [],
        username: ""
    }

    onChange = (plates, username) => {
        console.log('123', username);
        this.setState({
            plates, username
        });
    }
    render() {
        return (
            <AppContext.Provider value={{ plates: this.state.plates, username: this.state.username, onChange: this.onChange }}>
                {this.props.children}
            </ AppContext.Provider>
        )
    }

}
export default AppProvider;