import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Dimensions } from 'react-native'
import { Input, ListItem, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width: WIDTH } = Dimensions.get('window')

const textInput = {
    width: WIDTH / 2 - 25,
    height: 36,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 35,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 12
}
const inputIcon = {
    position: 'absolute',
    top: 9,
    left: 20
}
const inputContainer = { flexDirection: 'row', marginTop: 10 }

function AddUser({ loadData }) {
    const [isOpen, setVisiable] = useState(false);
    const [user, setUser] = useState({});
    const addUser = () => {
        setUser({ ...user, rule: "1" });
        if (user != {} && user != undefined) {
            // console.log(user);
            fetch('http://apismarttraffic.servehttp.com/users', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(user)
            }).then(response => response.json())
                .then((resJson) => {
                    alert("Đã thêm tài khoản");
                    loadData();
                    setVisiable(!isOpen);
                })
                .catch(alert("Không thể thêm tài khoản"))
        } else {
            alert("Xin kiểm tra lại")
        }
    }
    return (
        <View>
            <Button
                type="clear"
                containerStyle={{ width: 150 }}
                icon={
                    <Icon
                        name="plus-circle"
                        size={14}
                        color="#3399ff"
                    />
                }
                title="THÊM TÀI KHOẢN"
                onPress={() => { setVisiable(!isOpen) }}
            />
            {isOpen && (
                <View style={{ alignItems: 'center' }}>
                    <View style={inputContainer}>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Tên đăng nhập'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(username) => { setUser({ ...user, username }) }}
                            />
                        </View>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Mật khẩu'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(password) => { setUser({ ...user, password }) }}

                            />
                        </View>
                    </View>
                    <View style={inputContainer}>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Họ tên'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(name) => { setUser({ ...user, name }) }}
                            />
                        </View>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'CMND'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(CMND) => { setUser({ ...user, CMND }) }}
                            />
                        </View>
                    </View>
                    <View style={inputContainer}>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'SĐT'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(SDT) => { setUser({ ...user, SDT }) }}
                            />
                        </View>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Biển số xe'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(Plate) => { setUser({ ...user, Plate }) }}
                            />
                        </View>
                    </View>
                    <Button
                        title="Lưu"
                        type="solid"
                        icon={<Icon type='material-community' name='cloud-upload' color='#ffffff' />}
                        buttonStyle={{ width: 100, marginHorizontal: 30, marginTop: 15 }}
                        onPress={addUser}
                    />
                </View>
            )}
        </View>
    )

}
export default AddUser;