import React from 'react';
import { View, Text, Dimensions } from 'react-native'
import { PieChart } from 'react-native-chart-kit';
const { width: WIDTH } = Dimensions.get('window')

const chartConfig = {
    backgroundColor: "#e26a00",
    backgroundGradientFrom: "#fb8c00",
    backgroundGradientTo: "#ffa726",
    decimalPlaces: 2, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    style: {
        borderRadius: 16
    },
    propsForDots: {
        r: "6",
        strokeWidth: "2",
        stroke: "#ffa726"
    }
}
function Chart(props) {
    const data = [
        {
            name: "Vượt đèn",
            population: props.type1,
            color: "#ffcc00",
            legendFontColor: "#7F7F7F",
            legendFontSize: 15
        },
        {
            name: "Không nón",
            population: props.type2,
            color: "#3399ff",
            legendFontColor: "#7F7F7F",
            legendFontSize: 15
        },
        {
            name: "Cả 2 Lỗi",
            population: props.type0,
            color: "red",
            legendFontColor: "#7F7F7F",
            legendFontSize: 15
        }
    ];
    return (
        <View>
            <PieChart
                data={data}
                width={WIDTH}
                height={150}
                chartConfig={chartConfig}
                accessor="population"
                backgroundColor="transparent"
                absolute
            />

        </View>
    )

}
export default Chart;