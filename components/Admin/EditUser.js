import React, { useState } from 'react';
import { View, Image, Text, TextInput, Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
    ListItem, Icon, Card,
    Avatar, Overlay, Button
} from 'react-native-elements';
const { width: WIDTH } = Dimensions.get('window')


const textInput = {
    width: WIDTH / 2 - 25,
    height: 35,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 35,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 12
}
const inputIcon = {
    position: 'absolute',
    top: 4,
    left: 20
}
const inputContainer = {
    marginTop: 20,
    flexDirection: 'row'
}

function EditUser({ obj, loadData }) {
    const [isOpen, setVisiable] = useState(false);
    const [user, setUser] = useState(obj);
    const updateUser = (username) => {
        if (user != {} && user != undefined) {
            console.log(user);
            fetch(`http://apismarttraffic.servehttp.com/users/${username}`, {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(user)
            }).then(response => response.json())
                .then((resJson) => {
                    alert("Đã cập nhật thành công");
                    loadData();
                    setVisiable(!isOpen);
                })
                .catch(alert("Lỗi cập nhật"))
        } else {
            alert("Xin kiểm tra lại")
        }
    }
    const deleteUser = (username) => {
        fetch(`http://apismarttraffic.servehttp.com/users/${username}`, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => response.json())
            .then((resJson) => {
                if (resJson.message) {
                    loadData();
                    alert("Đã xóa");
                }
            })
            .catch(error => console.log(error))
    }
    return (
        <View>
            <ListItem
                key={obj._id}
                title={obj.name}
                subtitle={`CMND:${obj.CMND}`}
                rightTitle={obj.username}
                rightSubtitle={obj.SDT}
                leftAvatar={{ source: { uri: 'https://www.w3schools.com/howto/img_avatar.png' } }}
                borderRadius={20}
                bottomDivider
                onPress={() => { setVisiable(!isOpen) }}
                rightElement={
                    <Icon type='material-community'
                        name='delete-forever' color='red'
                        onPress={() => deleteUser(obj.username)}
                    />}
            />
            <Overlay
                isVisible={isOpen}
                fullScreen={true}
            >
                <View style={{ marginTop: 30, alignItems: 'center' }}>
                    <Text style={{ fontSize: 22 }}>THÔNG TIN TÀI KHOẢN</Text>
                    <View style={{ marginTop: 15 }}>
                        <Avatar
                            rounded
                            size='xlarge'
                            source={{
                                uri: 'https://www.w3schools.com/howto/img_avatar.png'
                            }}
                            showEditButton
                        />
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={inputContainer}>
                            <View >
                                <Ionicons name={'ios-person'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Họ tên"
                                    value={user.name}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setUser({ ...user, name: e })
                                    }}
                                />
                            </View>
                            <View>
                                <Ionicons name={'ios-pricetags'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Tên đăng nhập"
                                    value={user.username}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setUser({ ...user, username: e })
                                    }}
                                />
                            </View>
                        </View>
                        <View style={inputContainer}>
                            <View >
                                <Ionicons name={'ios-lock'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Mật khẩu"
                                    value={user.password}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setUser({ ...user, password: e })
                                    }}
                                />
                            </View>
                            <View>
                                <Ionicons name={'ios-pricetags'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="CMND"
                                    value={user.CMND}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setUser({ ...user, color: e })
                                    }}
                                />
                            </View>
                        </View>
                        <View style={inputContainer}>
                            <View >
                                <Ionicons name={'ios-barcode'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="SĐT"
                                    underlineColorAndroid='transparent'
                                    value={user.SDT}
                                    onChangeText={(e) => {
                                        setUser({ ...user, SDT: e })
                                    }}
                                />
                            </View>
                            <View>
                                <Ionicons name={'ios-barcode'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Biển số"
                                    underlineColorAndroid='transparent'
                                    value={obj.Plate.toString()}
                                    onChangeText={(e) => {
                                        setUser({ ...user, Plate: e })
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ marginTop: 25, flexDirection: 'row', marginLeft: 30 }}>
                        <Button
                            title="Đóng"
                            type="solid"
                            icon={<Icon type='material-community' name='close-circle-outline' color='#ffffff' />}
                            buttonStyle={{ width: WIDTH / 3, height: 40 }}
                            onPress={() => { setVisiable(!isOpen) }}
                        />
                        <Button
                            title="Cập nhật"
                            type="solid"
                            icon={<Icon size={25} type='material-community'
                                name='check' color='#ffffff' style={{ marginRight: 5 }} />}
                            buttonStyle={{ width: WIDTH / 3, height: 40, marginHorizontal: 30 }}
                            onPress={() => updateUser(obj.username)}
                        />

                    </View>
                </View>
            </Overlay>

        </View>
    )

}

export default EditUser;