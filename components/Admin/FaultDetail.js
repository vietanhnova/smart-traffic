import React, { useState, useEffect } from 'react';
import { View, Image, Text } from 'react-native';
import {
    ListItem, ButtonGroup, SearchBar,
    Badge, Overlay, Button
} from 'react-native-elements';
const unixTS = require('unix-timestamp');

getFaultByType = type => {
    switch (type) {
        case 0:
            return ('Vượt đèn đỏ + Không nón bảo hiểm');
            break;
        case 1:
            return ('Vượt đèn đỏ');
            break;
        case 2:
            return ('Không nón bảo hiểm');
            break;
        default:
            return '';
    }
}


function FaultDetail(obj) {
    // console.log("detail:",obj);
    const [isOpen, setVisiable] = useState(false);
    // const [count, setCount] = useState(0);
    // useEffect(() => { console.log("changedata", obj) })
    const getTypeStyle = type => {
        switch (type) { 
            case 0:
                return ({ backgroundColor: 'red' });
                break;
            case 1:
                return ({ backgroundColor: '#ffcc00' });
                break;
            case 2:
                return ({ backgroundColor: '#3399ff' });
                break;
            default:
                return {};
        }
    }
    const textStyle = {
        marginTop: 7,
        color: 'red'
    };
    const getImage = obj => {
        let unixStr = obj.date + " " + obj.time;
        let unixTime = unixTS.fromDate(unixStr);
        return (`http://apismarttraffic.servehttp.com/img/${obj.Plate}_${unixTime}.jpg`);
    }
    return (
        <View>
            <ListItem
                key={obj._id}
                title={obj.Plate}
                subtitle={`${obj.date} - ${obj.time}`}
                rightTitle={obj.user.length > 0 ? obj.user[0].name : ""}
                rightSubtitle={obj.user.length > 0 ? obj.user[0].SDT : ""}
                containerStyle={[getTypeStyle(obj.type), { flex: 1 }]}
                contentContainerStyle={{ flex: 1 }}
                borderRadius={20}
                bottomDivider
                chevron
                onPress={() => { setVisiable(!isOpen) }}
            />
            <Overlay
                isVisible={isOpen}
                fullScreen={true}
            >
                <View>
                    <View style={{ marginTop: 15 }}>
                        <Text style={{ textAlign: 'center', fontSize: 20 }}>CHI TIẾT LỖI VI PHẠM</Text>
                        <Image source={{ uri: getImage(obj) }}
                            style={{ width: '100%', height: 200, resizeMode: 'stretch', marginTop: 10 }} />
                        <Text style={{ marginTop: 15 }}>Thời gian : {obj.date} - {obj.time}</Text>
                        <Text style={textStyle}>Biển số : {obj.Plate}</Text>
                        <Text style={textStyle}>Lỗi vi phạm: {getFaultByType(obj.type)}</Text>
                        <Text style={{ marginTop: 7 }}>Chủ xe: {obj.user.length > 0 ? obj.user[0].name : ""}</Text>
                        <Text style={{ marginTop: 7 }}>CMND: {obj.user.length > 0 ? obj.user[0].CMND : ""}</Text>
                        <Text style={{ marginTop: 7 }}>SĐT: {obj.user.length > 0 ? obj.user[0].SDT : ""}</Text>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 15 }}>
                        <Button
                            title="Đóng"
                            type="solid"
                            buttonStyle={{ width: 170 }}
                            onPress={() => { setVisiable(!isOpen) }}
                        />
                    </View>
                </View>
            </Overlay>

        </View>
    )
}
export default FaultDetail;