import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Dimensions } from 'react-native'
import { Input, ListItem, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width: WIDTH } = Dimensions.get('window')

const textInput = {
    width: WIDTH / 2 - 25,
    height: 36,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 35,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 12
}
const inputIcon = {
    position: 'absolute',
    top: 9,
    left: 20
}
const inputContainer = { flexDirection: 'row', marginTop: 10 }

function AddVehicle({ loadData }) {
    const [isOpen, setVisiable] = useState(false);
    const [vehicle, setVehicle] = useState({});
    const addCar = () => {
        if (vehicle != {} && vehicle != undefined) {
            console.log(vehicle);
            fetch('http://apismarttraffic.servehttp.com/cars', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vehicle)
            }).then(response => response.json())
                .then((resJson) => {
                    console.log(resJson);
                    alert("Đã thêm xe");
                    loadData();
                    setVisiable(!isOpen);
                })
                .catch(alert("Không thể thêm xe"))
        } else {
            alert("Xin kiểm tra lại")
        }
    }
    return (
        <View>
            <Button
                type="clear"
                containerStyle={{ width: 150 }}
                icon={
                    <Icon
                        name="plus-circle"
                        size={14}
                        color="#3399ff"
                    />
                }
                title="THÊM XE MỚI"
                onPress={() => { setVisiable(!isOpen) }}
            />
            {isOpen && (
                <View style={{ alignItems: 'center' }}>
                    <View style={inputContainer}>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Hãng xe'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(label) => { setVehicle({ ...vehicle, label }) }}
                            />
                        </View>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Tên xe'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(name) => { setVehicle({ ...vehicle, name }) }}

                            />
                        </View>
                    </View>
                    <View style={inputContainer}>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Màu xe'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(color) => { setVehicle({ ...vehicle, color }) }}
                            />
                        </View>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Biển số'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(Plate) => { setVehicle({ ...vehicle, Plate }) }}
                            />
                        </View>
                    </View>
                    <View style={inputContainer}>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Chủ xe'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(manaUsername) => { setVehicle({ ...vehicle, manaUsername }) }}
                            />
                        </View>
                        <View>
                            <Icon name='tags' size={18} style={inputIcon} />
                            <TextInput
                                style={textInput}
                                placeholder={'Số khung'}
                                placeholderTextColor={'rgba(255,255,255,0.7)'}
                                underlineColorAndroid='transparent'
                                onChangeText={(number) => { setVehicle({ ...vehicle, number }) }}
                            />
                        </View>
                    </View>
                    <Button
                        title="Lưu"
                        type="solid"
                        icon={<Icon type='material-community' name='cloud-upload' color='#ffffff' />}
                        buttonStyle={{ width: 100, marginHorizontal: 30, marginTop: 15 }}
                        onPress={addCar}
                    />
                </View>
            )}
        </View>
    )

}
export default AddVehicle;