import React, { useState } from 'react';
import { View, Image, Text, TextInput, Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
    ListItem, Icon, Card,
    Avatar, Overlay, Button
} from 'react-native-elements';
const { width: WIDTH } = Dimensions.get('window')


const textInput = {
    width: WIDTH / 2 - 25,
    height: 35,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 35,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 12
}
const inputIcon = {
    position: 'absolute',
    top: 4,
    left: 20
}
const inputContainer = {
    marginTop: 20,
    flexDirection: 'row'
}

function EditVehicle({ obj, loadData }) {
    console.log(obj);
    const [isOpen, setVisiable] = useState(false);
    const [vehicle, setVehicle] = useState(obj.car);
    const updateVehicle = (plate) => {
        if (vehicle != {} && vehicle != undefined) {
            console.log(vehicle);
            fetch(`http://apismarttraffic.servehttp.com/cars/${plate}`, {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vehicle)
            }).then(response => response.json())
                .then((resJson) => {
                    alert("Đã cập nhật thành công");
                    loadData();
                    setVisiable(!isOpen);
                })
                .catch(alert("Lỗi cập nhật"))
        } else {
            alert("Xin kiểm tra lại")
        }
    }
    const deleteVehicle = (plate) => {
        fetch(`http://apismarttraffic.servehttp.com/cars/${plate}`, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => response.json())
            .then((resJson) => {
                if (resJson.message) {
                    loadData();
                    alert("Đã xóa");
                }
            })
            .catch(error => console.log(error))
    }
    return (
        <View>
            <ListItem
                key={obj.car._id}
                title={`${obj.car.label} ${obj.car.name}`}
                subtitle={`Biển số:${obj.car.Plate}`}
                rightTitle={obj.user.name}
                rightSubtitle={obj.car.number}
                borderRadius={20}
                bottomDivider
                onPress={() => { setVisiable(!isOpen) }}
                rightElement={
                    <Icon type='material-community'
                        name='delete-forever' color='red'
                        onPress={() => deleteVehicle(obj.car.Plate)}
                    />}
            />
            <Overlay
                isVisible={isOpen}
                fullScreen={true}
            >
                <View style={{ marginTop: 30, alignItems: 'center' }}>
                    <Text style={{ fontSize: 22 }}>THÔNG TIN PHƯƠNG TIỆN</Text>
                    <View style={{ marginTop: 15 }}>
                        <Avatar
                            rounded
                            size='xlarge'
                            source={{
                                uri:
                                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcStbkAGhLcUadUADIuDX0CumdWDXdo-kE2VoUx78975ZqLmlY_v',
                            }}
                            showEditButton
                        />
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={inputContainer}>
                            <View >
                                <Ionicons name={'ios-person'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Chủ xe"
                                    value={obj.user.name}
                                    underlineColorAndroid='transparent'
                                    editable={false}
                                />
                            </View>
                            <View>
                                <Ionicons name={'ios-pricetags'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Hãng xe"
                                    value={vehicle.label}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setVehicle({ ...vehicle, label: e })
                                    }}
                                />
                            </View>
                        </View>
                        <View style={inputContainer}>
                            <View >
                                <Ionicons name={'ios-bookmark'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Tên xe"
                                    value={vehicle.name}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setVehicle({ ...vehicle, name: e })
                                    }}
                                />
                            </View>
                            <View>
                                <Ionicons name={'ios-color-palette'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Màu sắc"
                                    value={vehicle.color}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(e) => {
                                        setVehicle({ ...vehicle, color: e })
                                    }}
                                />
                            </View>
                        </View>
                        <View style={inputContainer}>
                            <View >
                                <Ionicons name={'ios-barcode'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Biển số"
                                    underlineColorAndroid='transparent'
                                    value={vehicle.Plate}
                                    onChangeText={(e) => {
                                        setVehicle({ ...vehicle, Plate: e })
                                    }}
                                />
                            </View>
                            <View>
                                <Ionicons name={'ios-barcode'} size={28}
                                    color={'rgba(298,20,20,0.7)'} style={inputIcon} />
                                <TextInput
                                    style={textInput}
                                    placeholder="Số khung"
                                    underlineColorAndroid='transparent'
                                    value={vehicle.number}
                                    onChangeText={(e) => {
                                        setVehicle({ ...vehicle, number: e })
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ marginTop: 25, flexDirection: 'row', marginLeft: 30 }}>
                        <Button
                            title="Đóng"
                            type="solid"
                            icon={<Icon type='material-community' name='close-circle-outline' color='#ffffff' />}
                            buttonStyle={{ width: WIDTH / 3, height: 40 }}
                            onPress={() => { setVisiable(!isOpen) }}
                        />
                        <Button
                            title="Cập nhật"
                            type="solid"
                            icon={<Icon size={25} type='material-community'
                                name='check' color='#ffffff' style={{ marginRight: 5 }} />}
                            buttonStyle={{ width: WIDTH / 3, height: 40, marginHorizontal: 30 }}
                            onPress={() => updateVehicle(obj.Plate)}
                        />

                    </View>
                </View>
            </Overlay>

        </View>
    )

}

export default EditVehicle;