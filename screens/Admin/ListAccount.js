import React from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import AddUser from '../../components/Admin/AddUser';
import EditUser from '../../components/Admin/EditUser';
import AppProvider from '../../AppProvider';
import {
    ListItem, SearchBar
} from 'react-native-elements';

export default class ListVehicles extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tableData: [],
            search: "",
            isLoading: true,
        }
    }
    updateSearch = search => {
        console.log(search);
        this.setState({ search });
        const searchData = [];
        fetch("http://apismarttraffic.servehttp.com/users", {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                if (search != "") {
                    for (var i in responseJson) {
                        if (responseJson[i].CMND == search
                            || responseJson[i].SDT == search
                            || responseJson[i].name == search
                            || responseJson[i].username == search) {
                            searchData.push(responseJson[i]);
                        }
                    }
                    this.setState({ tableData: searchData });
                }
                else {
                    this.setState({ tableData: responseJson });
                }
            })
    };
    getData = () => {
        fetch("http://apismarttraffic.servehttp.com/users", {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    tableData: responseJson,
                    isLoading: false
                })
            })
            .catch(error => console.log(error))
    };
    componentDidMount() {
        this.getData();
    };
    render() {
        return (
            <View style={{ marginTop: 23, flex: 1 }}>
                <View>
                    <SearchBar
                        placeholder="Tìm kiếm "
                        onChangeText={this.updateSearch}
                        showLoading={this.state.isLoading}
                        onCancel={this.getData}
                        value={this.state.search}
                    />
                </View>
                <AddUser loadData={this.getData} />
                {this.state.isLoading ? null : <View style={{ flex: 1 }}>
                    <ScrollView>{
                        this.state.tableData.map((obj, i) => (
                            <EditUser obj={obj} key={i} loadData={this.getData} />
                        ))
                    }
                    </ScrollView>
                </View>}
            </View>
        )
    }

}