import React from 'react';
import {
    StyleSheet, View, Text, Image, TextInput,
    ImageBackground, Dimensions, TouchableOpacity
} from 'react-native';
import background from '../images/bg.jpeg';
import logo from '../images/logo.png'
import { Ionicons } from '@expo/vector-icons';
import AppProvider from '../AppProvider';

const { width: WIDTH } = Dimensions.get('window')
const { height: HEIGHT } = Dimensions.get('window')

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showPass: false,
            username: "admin",
            password: "admin"
        }
    }
    showPass = () => {
        if (this.state.showPass) {
            this.setState({ showPass: false })
        } else {
            this.setState({ showPass: true })
        }
    }
    navigate = (page) => {
        this.props.navigation.navigate(page, { username: this.state.username });
    }
    login = () => {
        fetch('http://apismarttraffic.servehttp.com/users/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
        }).then(response => response.json())
            .then((resJson) => {
                if (resJson.success) {
                    this.props.onChange(resJson.plate, this.state.username);
                    resJson.role == 0 ? this.navigate('Admin') : this.navigate('User');
                } else {
                    alert('Vui lòng thử lại!');
                }
            })
    }
    render() {
        return (
            <ImageBackground source={background} style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={logo} style={styles.logo} />
                    <Text style={styles.logoText}>SMART TRAFFIC</Text>
                </View>
                <View style={styles.inputContainer}>
                    <Ionicons name='ios-person' size={28} style={styles.inputIcon} />
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Tên đăng nhập'}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}
                        underlineColorAndroid='transparent'
                        onChangeText={(username) => this.setState({ username })}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Ionicons name='ios-lock' size={28} style={styles.inputIcon} />
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Mật khẩu'}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}
                        underlineColorAndroid='transparent'
                        secureTextEntry={this.state.showPass}
                        onChangeText={(password) => this.setState({ password })}
                    />
                    <TouchableOpacity style={styles.eyeBtn}
                        onPress={this.showPass.bind(this)} >
                        <Ionicons name={this.state.showPass ? 'ios-eye' : 'ios-eye-off'}
                            size={26} color={'rgba(255,255,255,0.7)'} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.loginBtn}
                    onPress={this.login.bind(this)}>
                    <Text style={styles.loginText}>Đăng nhập</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, alignItems: 'center' },
    logoContainer: { alignItems: 'center', marginTop: HEIGHT / 6 - 20 },
    logo: { width: 120, height: 120 },
    logoText: { color: 'white', fontSize: 20, marginTop: 10 },
    inputContainer: { marginTop: 10 },
    inputIcon: { position: "absolute", top: 6, left: 38 },
    textInput: {
        width: WIDTH - 85, height: 40, borderRadius: 25,
        fontSize: 16, paddingLeft: 45, backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)', marginHorizontal: 25
    },
    eyeBtn: { position: 'absolute', top: 8, right: 37 },
    loginBtn: {
        width: WIDTH - 200, height: 45,
        borderRadius: 25, justifyContent: 'center',
        backgroundColor: '#003366', marginTop: 20
    },
    loginText: {
        color: 'rgba(255,225,255,0.7)', fontSize: 16, textAlign: "center"
    }
});
const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <Login {...props} {...context}></Login> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;