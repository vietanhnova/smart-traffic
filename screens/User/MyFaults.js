import React, { useState } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions } from 'react-native';
import FaultDetail from '../../components/User/FaultDetail';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import AppProvider from '../../AppProvider';
import {
    ButtonGroup, SearchBar, Badge, Button
} from 'react-native-elements';
const screenWidth = Dimensions.get("window").width;

class MyFaults extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lastRefresh: Date(Date.now()).toString(),
            tableData: [],
            isLoading: true,
            selectedIndex: 0,
            countFails: {
                type0: 0,
                type1: 0,
                type2: 0
            },
            isVisible: false,
            showDatePicker1: false,
            showDatePicker2: false,
            fromDate: null,
            endDate: null,
            search: ''
        }
        this.updateIndex = this.updateIndex.bind(this)
    }
    showDatePicker1 = () => {
        this.setState({ showDatePicker1: true })
    }
    showDatePicker2 = () => {
        this.setState({ showDatePicker2: true })
    }
    appendLeadingZeroes = (n) => {
        if (n <= 9) {
            return "0" + n;
        }
        return n
    }
    updateFromDate = (date) => {
        let fromDate = date.getFullYear() + "/" + this.appendLeadingZeroes((date.getMonth() + 1)) + "/" + this.appendLeadingZeroes(date.getDate());
        this.setState({ fromDate: fromDate, showDatePicker1: false });
    }
    updateEndDate = (date) => {
        let endDate = date.getFullYear() + "/" + this.appendLeadingZeroes((date.getMonth() + 1)) + "/" + this.appendLeadingZeroes(date.getDate());
        this.setState({ endDate: endDate, showDatePicker2: false });
        this.getData();
        var dataByDate = [];
        for (var i in this.state.tableData) {
            if (Date.parse(this.state.fromDate) < Date.parse(this.state.tableData[i].date)
                && Date.parse(this.state.tableData[i].date) < Date.parse(endDate)) {
                dataByDate.push(this.state.tableData[i]);
            }
        }
        if (dataByDate.length >= 1 && dataByDate != undefined) {
            this.setState({ tableData: dataByDate });
        } else {
            alert('Không tìm thấy dữ liệu phù hợp');
        }
    }
    updateSearch = search => {
        this.setState({ search });
        const searchData = [];
        for (var i in this.props.plates) {
            fetch(`http://apismarttraffic.servehttp.com/fails/${this.props.plates[i]}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then(response => response.json())
                .then((responseJson) => {
                    if (search != "") {
                        for (var i in responseJson.data) {
                            if (responseJson.data[i].Plate == search
                                || responseJson.data[i].user[0].username == search
                                || responseJson.data[i].user[0].name == search) {
                                searchData.push(responseJson.data[i]);
                            }
                        }
                        this.setState({ tableData: searchData });
                    } else {
                        this.setState({ tableData: responseJson.data });

                    }
                })
                .catch(error => console.log(error))
        }

    };
    updateIndex(selectedIndex) {
        this.setState({ selectedIndex })
        switch (selectedIndex) {
            case 0:
                this.getData();
                break;
            case 1:
                this.getDataByType(2);
                break;
            case 2:
                this.getDataByType(1);
                break;
            case 3:
                this.getDataByType(0);
                break;
            default:
                return {};
        }
    }
    updateVisiable() {
        if (this.state.isVisible) {
            this.setState({ isVisible: false });
        } else {
            this.setState({ isVisible: true });
        }
    }
    getCountFails = () => {
        fetch("http://apismarttraffic.servehttp.com/fails/count", {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    countFails: responseJson
                })
            })
            .catch(error => console.log(error))
    }
    getDataByType = type => {
        const dataByType = [];
        for (var i in this.props.plates) {
            fetch(`http://apismarttraffic.servehttp.com/fails/${this.props.plates[i]}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then(response => response.json())
                .then((responseJson) => {
                    for (var i in responseJson.data) {
                        if (responseJson.data[i].type == type) {
                            dataByType.push(responseJson.data[i])
                        }
                    }
                    this.setState({ tableData: dataByType });
                })
                .catch(error => console.log(error))
        }
    }
    getData = () => {
        var data = [];
        for (var i in this.props.plates) {
            fetch(`http://apismarttraffic.servehttp.com/fails/${this.props.plates[i]}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then(response => response.json())
                .then((responseJson) => {
                    data = data.concat(responseJson.data);
                    this.setState({ tableData: data, isLoading: false });
                })
                .catch(error => console.log(error))
        }
    };
    componentDidMount() {
        this.getData();
    };
    allBtn = () => (<View>
        <Text>Tất cả</Text>
        <Badge
            status="success"
            value={this.state.countFails.type0 + this.state.countFails.type1 + this.state.countFails.type2}
        />
    </View>);
    helmetBtn = () => (<View>
        <Text>Không nón</Text>
        <Badge
            status="primary"
            value={this.state.countFails.type2}
        />
    </View>);
    redLightBtn = () => (<View>
        <Text>Vượt đèn</Text>
        <Badge
            status="warning"
            value={this.state.countFails.type1}
        />
    </View>);
    twoBtn = () => (<View>
        <Text>2 Lỗi</Text>
        <Badge
            status="error"
            value={this.state.countFails.type0}
        />
    </View>);
    buttons = [{ element: this.allBtn }, { element: this.helmetBtn }, { element: this.redLightBtn }, { element: this.twoBtn }]
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <SearchBar
                        placeholder="Tìm kiếm "
                        onChangeText={this.updateSearch}
                        showLoading={this.state.isLoading}
                        onCancel={this.getData}
                        value={this.state.search}
                    />
                    <View style={{ flexDirection: 'row', alignSelf: 'flex-start', marginTop: 5 }}>
                        <Text style={{ marginTop: 12 }}>Từ ngày :</Text>
                        <Button
                            title={this.state.fromDate ? `${this.state.fromDate}` : "Chọn ngày"}
                            type="clear"
                            buttonStyle={{ width: 115 }}
                            onPress={this.showDatePicker1}
                        />
                        <Text style={{ marginTop: 12 }}>Đến ngày :</Text>
                        <Button
                            title={this.state.endDate ? `${this.state.endDate}` : "Chọn ngày"}
                            type="clear"
                            buttonStyle={{ width: 115 }}
                            onPress={this.showDatePicker2}
                        />
                        <DateTimePickerModal
                            isVisible={this.state.showDatePicker1}
                            mode={"date"}
                            date={new Date()}
                            headerTextIOS="Chọn ngày"
                            confirmTextIOS="Xác nhận"
                            isDarkModeEnabled={true}
                            cancelTextIOS="Hủy"
                            onConfirm={this.updateFromDate}
                            onCancel={() => this.setState({ showDatePicker1: false })}
                        />
                        <DateTimePickerModal
                            isVisible={this.state.showDatePicker2}
                            mode={"date"}
                            headerTextIOS="Chọn ngày"
                            confirmTextIOS="Xác nhận"
                            isDarkModeEnabled={true}
                            cancelTextIOS="Hủy"
                            date={new Date()}
                            onConfirm={this.updateEndDate}
                            onCancel={() => this.setState({ showDatePicker2: false })}
                        />
                    </View>

                </View>
                <View>
                    <ButtonGroup
                        containerBorderRadius={3}
                        onPress={this.updateIndex}
                        selectedIndex={this.state.selectedIndex}
                        buttons={this.buttons}
                        containerStyle={{ width: screenWidth, left: 0 }}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    {this.state.isLoading ? null : <View style={{ flex: 1 }}>
                        <ScrollView>{
                            this.state.tableData.map((item, i) => (
                                <FaultDetail {...item} key={i} />
                            ))
                        }
                        </ScrollView>
                    </View>}
                </View>
            </View>

        )
    }

}
const styles = StyleSheet.create({
    container: { marginTop: 23, flex: 1 },
    buttonsContainer: { height: 42 }
});
const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <MyFaults {...props} {...context}></MyFaults> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;