import React from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import AddVehicle from '../../components/User/AddVehicle';
import EditVehicle from '../../components/User/EditVehicle';
import AppProvider from '../../AppProvider';
import {
    ListItem, SearchBar
} from 'react-native-elements';
import EditUser from '../../components/Admin/EditUser';

class MyVehicles extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tableData: [],
            search: "",
            isLoading: true,
        }
    }
    updateSearch = search => {
        this.setState({ search });
        const searchData = [];
        for (var i in this.props.plates) {
            fetch(`http://apismarttraffic.servehttp.com/cars/${this.props.plates[i]}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then(response => response.json())
                .then((responseJson) => {
                    if (search != "") {
                        if (responseJson.car.Plate == search
                            || responseJson.car.label == search
                            || responseJson.car.name == search) {
                            searchData.push(responseJson);
                        }

                        console.log("asd", searchData);
                        this.setState({ tableData: searchData });
                    } else {
                        this.setState({ tableData: responseJson });

                    }
                })
                .catch(error => console.log(error))
        }
    };
    getData = () => {
        var data = [];
        const plates = this.props.plates;
        for (var i in plates) {
            fetch(`http://apismarttraffic.servehttp.com/cars/${plates[i]}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then(response => response.json())
                .then((responseJson) => {
                    data = data.concat(responseJson);
                    this.setState({ tableData: data, isLoading: false });
                })
                .catch(error => console.log(error)) //to catch the errors if any
        }
    };
    componentDidMount() {
        this.getData();
    };
    render() {
        return (
            <View style={{ marginTop: 23 }}>
                <View>
                    <SearchBar
                        placeholder="Tìm kiếm "
                        onChangeText={this.updateSearch}
                        showLoading={this.state.isLoading}
                        onCancel={this.getData}
                        value={this.state.search}
                    />
                </View>
                <AddVehicle loadData={this.getData} />
                {this.state.isLoading ? null : <ScrollView>{
                    this.state.tableData.map((obj, i) => (
                        <EditVehicle obj={obj} key={i} loadData={this.getData} />
                    ))
                }
                </ScrollView>}
            </View>
        )
    }

}
const WrapperContext = (props) => {
    return (<AppProvider.Consumer>
        {context => { return <MyVehicles {...props} {...context}></MyVehicles> }}
    </AppProvider.Consumer>)
}
export default WrapperContext;