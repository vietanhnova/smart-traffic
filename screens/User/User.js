import React from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { BottomNavigation, Text } from 'react-native-paper';
import MyVehicles from './MyVehicles';
import MyFaults from './MyFaults';
import AccountInfo from '../AccountInfo';

export default class User extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'faults', title: 'Lỗi vi phạm', icon: 'traffic-light', color: '#3F51B5' },
            { key: 'vehicles', title: 'Phương tiện', icon: 'motorbike', color: '#009688' },
            { key: 'info', title: 'Tài khoản', icon: 'cogs', color: '#607D8B' },
        ]
    };
    _handleIndexChange = index => this.setState({ index });
    _renderScene = BottomNavigation.SceneMap({
        faults: MyFaults,
        vehicles: MyVehicles,
        info: AccountInfo
    });
    render() {
        return (
            <BottomNavigation
                navigationState={this.state}
                onIndexChange={this._handleIndexChange}
                renderScene={this._renderScene}
            />
        )
    }
}